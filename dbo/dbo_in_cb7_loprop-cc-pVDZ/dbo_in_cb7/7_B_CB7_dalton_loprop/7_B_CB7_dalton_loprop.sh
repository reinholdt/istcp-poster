#!/usr/bin/env bash
export PATH=/home/reinholdt/Programs/TheoDORE_1.7.1/bin:/home/reinholdt/miniconda3/bin:/home/reinholdt/cctools/bin:/opt/amber/amber18/bin:/usr/bin:/usr/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/opt/cuda/bin:/home/reinholdt/.local/share/flatpak/exports/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/opt/cuda/bin:/home/reinholdt/Programs/gromacs-2019/build/bin:/usr/bin:/usr/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/opt/cuda/bin:/home/reinholdt/.local/share/flatpak/exports/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:/home/reinholdt/Programs/dalton-private/pde-refactoring:/opt/schrodinger2019-1/:/home/reinholdt/Programs/orca_4_1_1_linux_x86-64_shared_openmpi313:/usr/local/bin:/home/reinholdt/Programs/gaussian09/g09
export LD_LIBRARY_PATH=/usr/lib:/usr/lib/gcc/x86_64-pc-linux-gnu/5.4.1/:/opt/amber/amber18/lib::/opt/cuda/lib:/opt/cuda/lib64:/home/reinholdt/Programs/orca_4_1_1_linux_x86-64_shared_openmpi313:/usr/local/lib
export DALTON_NUM_MPI_PROCS=12
export OMP_NUM_THREADS=1
export DALTON_TMPDIR=/tmp/PyFraME_r31gu9vu/7_B_CB7_dalton_loprop
mkdir -p $DALTON_TMPDIR
cd /home/reinholdt/Documents/istcp-poster/dbo/dbo_in_cb7_loprop-cc-pVDZ/dbo_in_cb7/7_B_CB7_dalton_loprop
dalton -d -noarch -nobackup -mb 2048 -get "AOONEINT DALTON.BAS SIRIFC AOPROPER RSPVEC" -o ../7_B_CB7_dalton_loprop.log -dal 7_B_CB7_dalton_loprop.dal -mol 7_B_CB7_dalton_loprop.mol
bzip2 --best /home/reinholdt/Documents/istcp-poster/dbo/dbo_in_cb7_loprop-cc-pVDZ/dbo_in_cb7/7_B_CB7_dalton_loprop.log
mv 7_B_CB7_dalton_loprop.AOONEINT AOONEINT
mv 7_B_CB7_dalton_loprop.DALTON.BAS DALTON.BAS
mv 7_B_CB7_dalton_loprop.SIRIFC SIRIFC
mv 7_B_CB7_dalton_loprop.AOPROPER AOPROPER
mv 7_B_CB7_dalton_loprop.RSPVEC RSPVEC
loprop -v -t . -A -a 2 --decimal 10 > /home/reinholdt/Documents/istcp-poster/dbo/dbo_in_cb7_loprop-cc-pVDZ/dbo_in_cb7/7_B_CB7_dalton_loprop.out
rm -f AOONEINT DALTON.BAS SIRIFC AOPROPER RSPVEC
