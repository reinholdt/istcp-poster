#!/usr/bin/env python

import pyframe
import sys
import pathlib

filename = sys.argv[1]
basename = str(pathlib.Path(filename).stem)
basis = sys.argv[2]

#use the pqr to be sure of the charge
system = pyframe.MolecularSystem(filename)
core = system.get_fragments_by_chain_id('A')
environment = system.get_fragments_by_chain_id('B')

system.set_core_region(fragments=core, basis=basis)

system.add_region(name="environment", fragments=environment, use_mfcc=True,
                    use_multipoles=True,
                    use_polarizabilities=True,
                    multipole_order=2,
                    multipole_model='LoProp',
                    polarizability_model='LoProp', 
                    multipole_method='HF',
                    polarizability_method='HF',
                    multipole_basis=basis,
                    polarizability_basis=basis)

project = pyframe.Project(mpi_procs_per_job=12,jobs_per_node=1)
project.memory_per_job = 12*2048 #memory per job is shared between mpi procs, so each mpi proc will have 2048 MB
project.scratch_dir = "/tmp"
project.work_dir = str(pathlib.Path(f'./{basename}_{basis}').absolute())
project.create_embedding_potential(system)
project.write_potential(system)
project.write_core(system)
