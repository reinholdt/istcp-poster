% Gemini theme
% https://github.com/anishathalye/gemini




\documentclass[final]{beamer}

% ====================
% Packages
% ====================

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[size=a0,orientation=portrait]{beamerposter}
\usetheme{gemini}
\usecolortheme{gemini}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{fontawesome}
\usepackage[numbers]{natbib}
\bibliographystyle{unsrtnat}


 
% ====================
% Lengths
% ====================

% If you have N columns, choose \sepwidth and \colwidth such that
% (N+1)*\sepwidth + N*\colwidth = \paperwidth
\newlength{\sepwidth}
\newlength{\colwidth}
\setlength{\sepwidth}{0.025\paperwidth}
\setlength{\colwidth}{0.3\paperwidth}

\newcommand{\separatorcolumn}{\begin{column}{\sepwidth}\end{column}}


\addtobeamertemplate{headline}{} 
{
    \begin{tikzpicture}[remember picture,overlay] 
    \node [anchor=north east, inner sep=3cm] at ([xshift=1.4cm,yshift=-5.0cm]current page.north east)     {\includegraphics[height=3.0cm]{figures/sdu_logo.pdf}}; 
    \end{tikzpicture} 

}

\addtobeamertemplate{headline}{} 
{
    \begin{tikzpicture}[remember picture,overlay] 
    \node [anchor=north west, inner sep=3cm] at ([xshift=0.0cm,yshift=0.0cm]current page.north west)     {\includegraphics[height=5.0cm]{figures/uit_logo.pdf}}; 
    \end{tikzpicture} 

}

\addtobeamertemplate{headline}{} 
{
    \begin{tikzpicture}[remember picture,overlay] 
    \node [anchor=north west, inner sep=3cm] at ([xshift=0.0cm,yshift=-5.0cm]current page.north west)     {\includegraphics[height=3.0cm]{figures/hylleraas_logo.pdf}}; 
    \end{tikzpicture} 

}


% ====================
% Title
% ====================

\title{MFCC-PDE: Polarizable Density Embedding for large biomolecules}

\author{Peter Reinholdt \inst{1}\inst{\phantom{x}} \and Frederik Kamper Jørgensen \inst{1} \and Jacob Kongsted \inst{1} \and Jógvan Magnus Haugaard Olsen \inst{2}}


\institute[shortinst]{\inst{1} University of Southern Denmark, Department of Chemistry, Physics and Pharmacy, DK-5230 Odense M, Denmark\\ \inst{2} Hylleraas Centre for Quantum Molecular Sciences, Department of Chemistry, UiT The Arctic University of Norway, Tromsø, N-9037, Norway\\ \inst{\phantom{x}} \\ \faTwitter @PeterReinholdt \quad  \faGithub peter-reinholdt \quad \faEnvelope reinholdt@sdu.dk }

% ====================
% Body
% ====================

\begin{document}

\begin{frame}[t]
\begin{columns}[t]
\separatorcolumn

\begin{column}{\colwidth}

  \begin{block}{Polarizable Embedding}

    In Polarizable Embedding (PE), the conventional Fock/Kohn-Sham operator is augmented by an embedding operator
    \begin{equation}
        \hat{f}^{\mathrm{eff}} = \hat{f}^{\mathrm{vac}} + \hat{v}^\mathrm{emb}.
    \end{equation}

    The PE contributions consist of multipole-expanded electrostatic and induction terms

    \begin{equation}
        \hat{v}^{\mathrm{emb}} = \hat{v}^\mathrm{es} + \hat{v}^\mathrm{ind}
    \end{equation}

    where the electrostatic operator is

    \begin{equation}
        \hat{v}_{\mu\nu}^{\mathrm{es}} =  \sum_{s=1}^{S} \sum_{|k| = 0}^{K_s} \frac{(-1)^{|k|}}{k!} \boldsymbol{M}_s^{(k)} \boldsymbol{t}_{\mu\nu}^{(k)}(\mathbf{R}_s) 
    \end{equation}

    while the induction operator is

    \begin{equation}
        \hat{v}_{\mu\nu}^{\mathrm{ind}} = - \sum_{s=1}^{S} \boldsymbol{\mu}^{\mathrm{ind}}_{s} \boldsymbol{t}_{\mu\nu}^{(1)} (\mathbf{R}_{s})
    \end{equation}

    The induced dipoles are found from the solution of

    \begin{equation}
        \textbf{B} \boldsymbol{\mu}^{\mathrm{ind}} = \textbf{F}
    \end{equation}

    \begin{figure}
      \centering
      \includegraphics[width=1.0\columnwidth]{figures/PE-example.pdf}
      \caption{PE potential consists of atomically distributed multpoles and dipole--dipole polarizabilties.}
    \end{figure}


  \end{block}

  \begin{block}{Polarizable Density Embedding}
     
    In Polarizable Density Embedding (PDE), the embedding operator consists of three
      parts \cite{olsen2015polarizable}

    \begin{equation}
        \hat{v}^{\mathrm{emb}} = \hat{v}^\mathrm{fd} + \hat{v}^\mathrm{ind} + \hat{v}^{\mathrm{rep}}
    \end{equation}


    The electrostatic operator from PE is treated exactly instead of relying on a multipolar expansion

    \begin{equation}
        \hat{v}^{\mathrm{fd}}_{\mu\nu} = \sum_{a=1}^{N_a} \sum_{\gamma\delta \in a} D_{\gamma \delta} v_{\mu\nu,\gamma\delta}
        - \sum_{a=1}^{N_a} \sum_{m \in a} Z_m v_{\mu\nu}(\mathbf{R}_m)
    \end{equation}

    and further, non-electrostatic repulsion is modeled by means of a projection 
    operator which models the wave-function orthogonality between the core region
    and fragments in the environment

    \begin{equation}
        \hat{v}^{\mathrm{rep}}_{\mu\nu} = - \sum_{a=1}^{N_a} \sum_{\gamma\delta \in a} S_{\mu\gamma} S_{\nu\delta} \sum_{i=1}^{N_{occ,a}} \varepsilon_i C_{\gamma i} C_{\delta i}
    \end{equation}


  \end{block}

  \begin{alertblock}{Treating large systems}
      The Molecular Fractionation with Conjugate Caps (MFCC) method 
      enables calculations on large systems by splitting up the complete system into several smaller
      fragments. 
      This approach has been used to derive atom-centered parameters for proteins\cite{Soderhjelm2009}
      
      To apply the PDE model to large systems, we apply the MFCC procedure at 
      a Fock matrix level as

      \begin{equation}
          F_{\mu\nu}^{\mathrm{fd+rep}} = \sum_{f=1}^{F} F_{\mu\nu}^{\mathrm{fd+rep},f} - \sum_{c=1}^{C} F_{\mu\nu}^{\mathrm{fd+rep},c}
      \end{equation}


    \begin{figure}
      \centering
      \includegraphics[width=1.0\columnwidth]{figures/pde-mfcc.png}
        \caption{A dipeptide is split into two capped fragments and a conjugate cap}
    \end{figure}

  \end{alertblock}

\end{column}

\separatorcolumn

\begin{column}{\colwidth}

  \begin{block}{Implementation}

    We use a brand-new PDE implementation which features much easier setup using the PyFraME python package.
    
      \begin{figure}
          \centering
      \includegraphics[width=1.0\columnwidth]{figures/pde-workflow.pdf}
          \caption{Workflow in a PDE calculation.}
    \end{figure}

  \end{block}

  \begin{block}{ESP validation}
    
      The MFCC-PDE has better reproduction of the electrostatic potential (ESP) than
      the PE model, in particular in the short-range.

    \begin{figure}
      \centering
      \includegraphics[width=0.75\columnwidth]{figures/diglycine_esp.pdf}
      \caption{Error in the ESP relative to a CAM-B3LYP/aug-cc-pVDZ reference on two vdW surfaces. Note the scale on the color bars.}
    \end{figure}

        The ESP is also reproduced well by MFCC-PDE on larger systems, for example on an insulin protein.
        The reference ESP was obtained from \citeauthor{jakobsen2013electrostatic}\cite{jakobsen2013electrostatic}.

    \begin{figure}
      \centering
      \includegraphics[width=1.0\columnwidth]{figures/insulin_esp.pdf}
      \caption{With an MP2/cc-pVDZ ESP as a reference, errors in the ESP obtained with either PDE, PE or full-QM CAM-B3LYP are visualized on molecular surfaces.}
    \end{figure}

  \end{block}

    \begin{block}{Application: Excitation energies of \textit{para}-benzoquinone}
   
        We calculate excitation energies of the \textit{para}-benzoquinone (pBQ) molecule in a diglycine environment.
        The environment is represented with either MFCC-PDE or PE. 
        A supermolecular CAM-B3LYP/aug-cc-pVDZ calculation is used as a reference.
        MFCC-PDE reproduces the excitation energies with higher accuracy than the standard PE model.

    \begin{table}
      \centering
      \begin{tabular}{l c c c r}
        \toprule
          \textbf{State} & \textbf{Reference} & \textbf{MFCC-PDE} & \textbf{PE} & \textbf{Character} \\ 
        \midrule
          1 & 3.059 & 3.062 & 3.046 & $n\rightarrow \pi^*$ \\
          2 & 3.786 & 3.766 & 3.830 & $\pi\rightarrow \pi^*$ \\
          3 & 4.377 & 4.259 & 4.228 & $\pi\rightarrow \pi^*$ \\
          4 & 5.470 & 5.481 & 5.444 & $\pi\rightarrow \pi^*$ \\
        \bottomrule
      \end{tabular}
        \caption{Excitation energies (in eV) of the pBQ molecule with a diglycine environment.}
    \end{table}
    

    \begin{figure}
        \centering
        \includegraphics[width=0.50\columnwidth]{figures/pbq_in_GG.png}
        \caption{The pBQ molecule with a diglycine environment.}
    \end{figure}


  \end{block}

\end{column}

\separatorcolumn

\begin{column}{\colwidth}

    \begin{block}{Application: DBO in CB[7]}
    
        Previous studies\cite{marefat2018analytic} have revealed severe charge-leak
        problems when using a conventional PE formulation in the host-guest complex
        of 2,3-diazabicyclo-[2.2.2]oct-2-en (DBO) inside cucurbit[7]uril (CB[7]).

        \begin{figure}
            \centering
            \includegraphics[width=0.5\columnwidth]{figures/dbo_in_cb7.png}
            \caption{The DBO in CB[7] host-guest complex.}
        \end{figure}

        We can address this problem efficiently with the MFCC-PDE model.
        By comparing PE-TDHF and PDE-TDHF excitation energies to a supermolecular TDHF 
        reference calculation, we see that severe charge-leaking occurs when using diffuse basis sets
        with the standard PE model.
        This is efficiently solved by using the MFCC-PDE model.
        
        \begin{table}
        \begin{tabular}{c|cc|cc|cc}
            \toprule
            \textbf{Method} & \multicolumn{2}{c|}{\textbf{PE}} & \multicolumn{2}{c|}{\textbf{PDE}} & \multicolumn{2}{c}{\textbf{Supermolecule}}\tabularnewline
            \textbf{State/Basis} & \textbf{cc-pVDZ} & \textbf{+aug} & \textbf{cc-pVDZ } &  \textbf{+aug } & \textbf{cc-pVDZ } & \textbf{ +aug} \tabularnewline
            \midrule
            \multicolumn{1}{c}{1} & 3.592 & \multicolumn{1}{c}{\textbf{1.503}} & 3.544 & \multicolumn{1}{c}{3.553} & 3.496 & 3.504\tabularnewline
            \multicolumn{1}{c}{2} & 6.617 & \multicolumn{1}{c}{\textbf{1.829}} & 6.566 & \multicolumn{1}{c}{6.562} & 6.465 & 6.471\tabularnewline
            \bottomrule
        \end{tabular}
            \caption{TDHF excitation energies (eV) of DBO in CB[7].}
        \end{table}

        The PDE model is efficiently formulated and is only slightly more expensive than the corresponding PE calculation.
        \begin{table}
\begin{tabular}{rrcccc}
\toprule
\textbf{Basis} & \textbf{Method} & \textbf{Potential} & \textbf{TDHF} & \textbf{Total} & \textbf{Nodes}\tabularnewline
\midrule
cc-pVDZ & PE & 00:20:57 & 00:00:35 & 00:21:32 & 1\tabularnewline
cc-pVDZ & PDE & 00:41:16 & 00:00:48 & 00:42:04 & 1\tabularnewline
cc-pVDZ & Supermolecule & -- & 04:58:04 & 04:58:04 & 16\tabularnewline
aug-cc-pVDZ & PE & 00:54:41 & 00:03:25 & 00:58:06 & 1\tabularnewline
aug-cc-pVDZ & PDE & 01:45:31 & 00:02:02 & 01:47:33 & 1\tabularnewline
aug-cc-pVDZ & Supermolecule & -- & 16:52:26 & 16:52:26 & 16\tabularnewline
\bottomrule
\end{tabular}
            \caption{Timings of PE-TDHF, PDE-TDHF and supermolecular TDHF calculations on DBO in CB[7]. One node contains 2x E5-2680 v3 (24 cores total).} 
        \end{table}



  \end{block}

  \begin{block}{Application: Nile red in $\beta$-lactoglobulin}

The MFCC fragmentation scheme allows for treating large environments.
Since MFCC-PDE is formulated with the framework of molecular response theory,
a wide range of molecular properties can be calculated.

      \begin{figure}
          \centering
          \includegraphics[width=0.6\textwidth]{figures/nir.png}
          \caption{The nile red chromophore inside the $\beta$-lactoglobulin protein.}
     \end{figure}

Optical properties of nile red were calculated with CAMB3LYP/aug-cc-pVDZ for the five lowest excited states.

     \begin{table}
         \centering
\begin{tabular}{rrrrr|rrrr}
\toprule
 & \multicolumn{4}{c|}{PE} & \multicolumn{4}{c}{PDE}\tabularnewline
\textbf{State} & \textbf{$E_{\mathrm{exc}}$} & \textbf{$f^{\mathrm{1PA}}$} & \textbf{$\sigma^{\mathrm{2PA}}$} & \textbf{$\delta^{\mathrm{3PA}}$} & \textbf{$E_{\mathrm{exc}}$} & \textbf{$f^{\mathrm{1PA}}$} & \textbf{$\sigma^{\mathrm{2PA}}$} & \textbf{$\delta^{\mathrm{3PA}}$}\tabularnewline
\midrule
1 & 2.710 & 1.093 & 49.1 & 0.133  & 2.744 & 0.658 & 46.9 & 0.665\tabularnewline
2 & 3.192 & 0.003 & 5.5  & 0.087 & 3.579 & 0.001  & 7.7 & 0.633\tabularnewline
3 & 3.572 & 0.003 & 12.2 & 0.185  & 3.670 & 0.003 & 8.8 & 0.871\tabularnewline
4 & 3.621 & 0.001 & 0.6  & 0.003 & 3.807 & 0.021  & 0.1 & 0.020\tabularnewline
5 & 3.683 & 0.004 & 2.8  & 0.064 & 4.267 & 0.003  & 235 & 35.9\tabularnewline
\bottomrule 
\end{tabular}







         \caption{The excitation energies (eV) and associated one- (au), two- (in GM) and three-photon ($10^{9}\times$ au) absorption strengths of nile red is computed in a protein environment.}
     \end{table}
  \end{block}

  \begin{block}{References}
    \footnotesize{\bibliography{\jobname}}

  \end{block}

\end{column}

\separatorcolumn
\end{columns}
\end{frame}

\end{document}
